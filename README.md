# LimitYourPresence

Now COVID-19 still imposes limitations on the way we come together, I see a lot of places in need of a reservation system.

*  At work, we can't be more than 20 persons in the landscape office, and the lab doesn't allow more than 4, meeting room 1 is limited to 6 and meeting room 2 limited to 9 persons at a time.
* In the hospital, a patient can't have more than 2 visitors at a time, less than 3 visits per week; the whole wing can't have more than 16 visitors at any moment.  At the geriatric wing where my dad lies, they work with a hand brew spreadsheet managed by one nurse.
* Restaurants and pubs require reserving a spot to keep the number of people under 1/8m²; when a reservation is canceled, another family of 5 can be allowed in.  Some are very inventive and [use an old traffic light].
* Hotels need to adapt their systems, too

It's a pity that everyone would have to invent this on their own.  So I went searching the web (open source, reservation, COVID, tools).  I bumped onto this [github overview].  I read through this [Singapore digital tools] overview.

But I couldn't find a tool to solve this very common and current problem.  Am I too optimistic, and can't this problem be generalized?  Or hasn't the need been urgent enough?  Something else?

I wonder... could we join efforts solving this?  The result is certainly something that's going to last.  Experts expect new viruses coming along, and for sure, COVID isn't dead yet.

## Requirements

Some preliminary requirements I can think of:

* The System can be told what the constraints are
* The System can be accessed from multiple points
* Current schedule can be checked against the constraints
* Constraints can change, and involved parties have to be notified
* The System can visualize time occupancy over space, or space occupancy over time

[github overview]: https://github.com/soroushchehresa/awesome-coronavirus
[Singapore digital tools]: https://sgtechcentre.undp.org/content/sgtechcentre/en/home/digital-tools-for-covid-19.html
[use an old traffic light]: https://www.tvoost.be/nieuws/coronavirus-verkeerslicht-regelt-toegang-in-cafe-de-smoutpot-in-melsele-99763
